# Evaluations

## Examples

1. Interactive storytelling (storytelling pattern)
1. Listening (backchanneling pattern)
1. EMAR/DBT squared breathing (instruction pattern)
1. EMAR/DBT multi-robot task (storytelling + reactive pattern)

### Folders

```
examples/
  interactive_storytelling/ (interactive storytelling)
  storylistener/ (back-channeling)
  emar_squared_breathing/
  emar_emotional_clarity/
  emar_wisemind_charades/
```

### Example folder contents

- a description of the task/interaction
  - in FSM for precision, if helpful
- a human simulator for modeling human behavior
  - test (spec-based or unit)

## Case Studies

1. the the EMAR team
1. with Kyle, David, and Andrew at U.Wisc.
1. https://robotsforgood.yale.edu/

model after enima's rosette paper's case study section

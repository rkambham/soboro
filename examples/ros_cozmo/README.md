# ros_cozmo example

1. Install [ros_cozmo](https://github.com/mjyc/ros_cozmo) and run it
2. Run
    ```
    npm install
    # create a program
    # node ../../cli.js programs/{select_program}.sbr
    node src/run.js
    ```

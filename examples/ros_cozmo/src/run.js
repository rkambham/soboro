const xs = require("xstream").default;
const { run } = require("@cycle/run");
const makeROSDriver = require("./makeROSDriver");
const { interpret } = require("soboro");

const programJSON = require("./program.json") || [];

const main = (sources) => {
  sources.ROS.addListener({ next: () => {} }); // start the ROS driver
  const programSources = {
    // derive "started" from a cozmo event
    started: xs
      .periodic(1000)
      .take(1)
      .mapTo(true),
  };
  const programSinks = interpret(programJSON, programSources);
  const sinks = {
    ROS: programSinks.say.map((x) => ({
      type: "action",
      value: {
        name: "/say_text",
        goalMessage: {
          text: x,
          use_cozmo_voice: true,
          duration_scalar: 1.0,
          voice_pitch: 0.0,
        },
      },
    })),
  };
  return sinks;
};

const drivers = {
  ROS: makeROSDriver({
    topics: [
      {
        name: "/started",
        messageType: "std_msgs/Bool",
      },
    ],
    actions: [
      {
        serverName: "/say_text",
        actionName: "ros_cozmo/SayText",
      },
    ],
  }),
};

run(main, drivers);

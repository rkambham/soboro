const rosnodejs = require("rosnodejs");
const xs = require("xstream").default;
const fromEvent = require("xstream/extra/fromEvent").default;

const makeROSDriver = (options) => {
  if (!options) {
    options = {};
  }
  if (!options.rosNodeName) {
    options.rosNodeName = "/cyclejs_ros_driver";
  }
  if (!options.topics) {
    options.topics = [];
  }
  // add "services"
  if (!options.actions) {
    options.actions = [];
  }
  // check for connection

  const ROSDriver = (outgoing$) => {
    const incoming$ = xs.create({
      start: (listener) => {
        // lazy "initNode", i.e., initialize on subscribe
        rosnodejs.initNode(options.rosNodeName).then((nh) => {
          const acs = {};
          options.actions.map((options) => {
            acs[options.serverName] = new rosnodejs.ActionClient({
              nh,
              type: options.actionName,
              actionServer: options.serverName,
            });
          });
          outgoing$.addListener({
            next: (outgoing) => {
              if (outgoing.type === "action")
                // create outgoing streams from returned "handle"
                //   https://github.com/RethinkRobotics-opensource/rosnodejs/blob/3.0.2/src/actions/ActionClient.js
                acs[outgoing.value.name].sendGoal(outgoing.value.goalMessage);
              else console.warn("Unknown type", outgoing.type);
            },
          });

          const topics = {};
          options.topics.map((topicOptions) => {
            nh.subscribe(topicOptions.name, topicOptions.messageType, (msg) => {
              listener.next({
                type: "topic",
                value: { name: topicOptions.name, message: msg },
              });
            });
          });
        });
      },
      stop: () => {},
    });

    return incoming$;
  };

  return ROSDriver;
};

module.exports = makeROSDriver;

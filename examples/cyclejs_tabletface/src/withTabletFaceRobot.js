const xs = require("xstream").default;
const { withState } = require("@cycle/state");
const { withTabletFaceRobotActions } = require("@cycle-robot-drivers/run");

const ProgramSourceNames = ["started", "sayDone", "buttonClicked"];
const ProgramSinkNames = ["say", "displayButtons"];

const withTabletFaceRobot = (main, { displayPoseViz = true } = {}) => {
  return (sources) => {
    const options = {
      hidePoseViz: !displayPoseViz,
      styles: {
        robotSpeechbubble: {
          styles: {
            message: {
              fontSize: "10vmin",
            },
          },
        },
      },
    };
    const adapt = (main) => {
      return (sources) => {
        const sayDone = sources.SpeechSynthesisAction.result
          .debug()
          .map((x) => x.result);
        const buttonClick = sources.HumanSpeechbubbleAction.result.map(
          (x) => x.result
        );
        const childSources = Object.assign({ sayDone, buttonClick }, sources);
        const childSinks = main(childSources);
        const humanSpeechbubbleAction$ = {
          goal: childSinks.displayButtons || xs.never(),
        };
        const speechSynthesisAction$ = {
          goal: childSinks.say || xs.never(),
        };

        const data$ = xs.merge(
          ...["started", "sayDone", "buttonClick"].map((name) => {
            return childSources[name].map((value) => {
              return {
                type: "source",
                name,
                stamp: Date.now(), // replace it
                value,
              };
            });
          }),
          ...["displayButtons", "say"].map((name) => {
            return childSinks[name].map((value) => {
              return {
                type: "sink",
                name,
                stamp: Date.now(), // replace it
                value,
              };
            });
          })
        );
        const sinks = {
          HumanSpeechbubbleAction: humanSpeechbubbleAction$,
          SpeechSynthesisAction: speechSynthesisAction$,
          data: data$,
        };
        return sinks;
      };
    };
    const childSinks = withState(
      withTabletFaceRobotActions(adapt(main), options)
    )(sources);
    const sinks = childSinks;
    return sinks;
  };
};

module.exports = {
  default: withTabletFaceRobot,
  ProgramSourceNames,
  ProgramSinkNames,
};

// ===========
// interpreter
// ===========

const pegjs = require("pegjs");

const grammar = `
Rules
  = _ rule:Rule rules:(BR Rule)* _ {
    return rules.reduce((acc, x) => acc.concat([x[1]]), [rule]);
  }

Rule
  = "+" WS trigger:EventPattern WS BR actions:Actions {
    return { trigger, actions }
  }

EventPattern
  = expr:Character+ {
    return expr.join("");
  }

Actions
  = WS action:Action actions:(BR Action)* WS {
    return actions.reduce((acc, x) => acc.concat([x[1]]), [action]);
  }

Action
  = "-" WS name:ActionName "=" value:ActionValue {
    return { name, value };
  }

ActionName =
  name:Character+ {
    return name.join("");
  }

ActionValue =
  value:String {
    return value;
  }

// Boolean = ("f" / "t") {
//   return text() === "t";
// }

String
  = '"' chars:(Character / Number)* '"' {
    return chars.join("");
  }

Character = [a-zA-Z .]

Number = [0-9] {
  return parseInt(text(), 10);
}

_ = [ \\t\\r\\n]*

WS = [ \\t]*

BR = [\\r\\n]+
`;

const parser = pegjs.generate(grammar);

// =========
// interpret
// =========

const xs = require("xstream").default;

const interpretTriggerExpression = (exp, sources) => {
  if (exp.op === "equal") {
    return sources[exp.value[0]].filter((x) => x === exp.value[1]).mapTo(true);
  } else if (exp.op === "or") {
    return xs.merge(...exp.value.map((x) => sources[x]));
  } else {
    return sources[exp];
  }
};

// throws an exception is sources do not contain a variable used in prog
const interpret = (prog, sources) => {
  const sinks = {};
  prog.map((rule) => {
    rule.actions.map((action) => {
      const trigger$ = interpretTriggerExpression(rule.trigger, sources);
      sinks[action.name] =
        typeof sinks[action.name] === "undefined"
          ? trigger$.mapTo(action.value)
          : (sinks[action.name] = xs.merge(
              sinks[action.name],
              trigger$.mapTo(action.value)
            ));
      return sinks;
    });
  });
  return sinks;
};

const makeProgramExecutor = ({
  // ProgramSourceNames = [],
  ProgramSinkNames = [],
} = {}) => {
  const programSinkNames = ProgramSinkNames;

  // doesn't support queuing
  const ProgramExecutor = (sources) => {
    const intent$ = sources.program;
    const model$ = intent$.fold(
      (prev, intent) => {
        // intent === null means cancel
        if (prev.status === "ready" && intent !== null) {
          let childSinks = {};
          try {
            childSinks = interpret(intent, sources);
          } catch (err) {
            console.error(err);
          }
          const sinks = programSinkNames.reduce((prev, name) => {
            prev[name] = childSinks[name] || xs.never();
            return prev;
          }, {});
          return {
            status: "running",
            sinks,
          };
        } else if (prev.status === "running" && intent === null) {
          const sinks = programSinkNames.reduce((prev, name) => {
            prev[name] = xs.never();
            return prev;
          }, {});
          return {
            status: "ready",
            sinks,
          };
        }
        return prev;
      },
      {
        status: "ready",
        sinks: programSinkNames.reduce((prev, name) => {
          prev[name] = xs.never();
          return prev;
        }, {}),
      }
    );

    const sinks = programSinkNames.reduce((prev, name) => {
      prev[name] = model$
        .map((model) => model.sinks[name] || xs.never())
        .flatten();
      return prev;
    }, {});
    return sinks;
  };

  return ProgramExecutor;
};

// ============
// fsSyncDriver
// ============

const fs = require("fs");
// const xs = require("xstream").default;

const fsSyncDriver = (command$) => {
  let sub;
  const producer = {
    start: (listener) => {
      sub = command$.subscribe({
        next: (command) => {
          if (!command.method || !command.method.endsWith("Sync")) {
            console.warn("Invalid command.method", command.method);
            return;
          }
          try {
            const result = fs[command.method].apply(fs, command.args);
            listener.next(result);
          } catch (err) {
            listener.error(err);
          }
        },
      });
    },
    stop: () => {
      sub && sub.unsubscribe();
    },
  };
  return xs.create(producer);
};

// ===============
// firestoreDriver
// ===============

// const xs = require("xstream").default;
const firebase = require("firebase/app");
require("firebase/firestore");

const makeFirestoreDriver = (firebaseConfig) => {
  if (!firebase.apps.length) firebase.initializeApp(firebaseConfig);
  const db = firebase.firestore();

  // doesn't support "category" yet
  const firestoreDriver = (commands$) => {
    let listener;
    const sub = commands$.subscribe({
      next: (commands) => {
        let cur = db;
        for (let i = 0; i < commands.length; i++) {
          const command = commands[i];

          if (command.method === "onSnapshot") {
            let unsubscribe;
            const producer = {
              start: (listener) => {
                unsubscribe = cur.onSnapshot({
                  next: (x) => listener.next(x),
                  error: (x) => listener.error(x),
                  complete: (x) => listener.complete(x),
                });
              },
              stop: () => {
                unsubscribe && unsubscribe();
              },
            };
            listener && listener.next(xs.create(producer));
            if (i !== commands.length - 1)
              console.warn(
                "break after onSnapshot; remaining commands",
                commands.slice(i + 1)
              );
            break;
          }

          cur = cur[command.method](...command.args);
          if (cur.then) {
            listener && listener.next(xs.from(cur));
            if (i !== commands.length - 1)
              console.warn(
                "break on Promise; remaining commands",
                commands.slice(i + 1)
              );
            break;
          }
        }
      },
    });

    const producer = {
      start: (x) => {
        listener = x;
      },
      stop: () => {
        sub.unsubscribe();
      },
    };
    return xs.create(producer);
  };
  return firestoreDriver;
};

module.exports = {
  parser,
  interpret,
  makeProgramExecutor,
  fsSyncDriver,
  makeFirestoreDriver,
};

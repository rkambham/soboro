const xs = require("xstream").default;
const { div, button, span, textarea } = require("@cycle/dom");

const intent = (domSource) => {
  return domSource
    .select("textarea.program-editor")
    .events("input")
    .map((ev) => ({
      type: "update_program",
      value: {
        type: ev.target.dataset.type,
        value: ev.target.value,
      },
    }));
};

const model = (action$) => {
  return action$.fold(
    (prev, input) => {
      if (input.type === "update_program")
        return { rawProgramTxt: input.value.value };
      return prev;
    },
    {
      rawProgramTxt: "",
    }
  );
};

const events = (sources) => {
  const events$ = xs.merge(
    sources.DOM.select("textarea.program-editor")
      .events("input")
      .map((ev) => ({
        type: "text",
        value: ev.target.value,
      })),
    sources.DOM.select(".run")
      .events("click")
      .map((ev) => ({
        type: "run",
      })),
    sources.DOM.select(".stop")
      .events("click")
      .map((ev) => ({
        type: "stop",
      }))
  );
  return events$;
};

const view = (state$) => {
  const styles = {
    body: {
      display: "flex",
      minHeight: "100vh",
      flexDirection: "column",
    },
    navbar: {
      height: "30px",
      background: "#d9d9d959",
      display: "flex",
      alignItems: "center",
    },
    main: {
      flex: 1,
      background: "#d9d9d959",
      padding: "5px",
      display: "flex",
      flexDirection: "row",
    },
    col: {
      flexGrow: 1,
      display: "flex",
      flexDirection: "column",
    },
    pane: {
      flex: 1,
      display: "flex",
    },
    pane_textarea: {
      resize: "none",
      flex: 1,
      margin: "0px 5px 5px 0px",
      fontFamily: "Source Code Pro Regular, Menlo Regular, Monospace",
    },
    navbar_span_btnGroup: {
      margin: "0 5px",
    },
  };
  return state$.map((state) =>
    div(".body", { style: styles.body }, [
      div(".navbar", { style: styles.navbar }, [
        span(".btn-group", { style: styles.navbar_span_btnGroup }, [
          button(".run", "run"),
          button(".stop", "stop"),
        ]),
      ]),
      div(".main", { style: styles.main }, [
        div(".col", { style: styles.col }, [
          div(".pane", { style: styles.pane }, [
            textarea(
              ".program-editor",
              { style: styles.pane_textarea },
              {
                attrs: {
                  placeholder: `Your program here`,
                  "data-type": `input`,
                },
              },
              state.rawProgramTxt
            ),
          ]),
        ]),
      ]),
    ])
  );
};

const Editor = (sources) => {
  const action$ = intent(sources.DOM);
  const state$ = model(action$);
  const vdom$ = view(state$);
  const events$ = events(sources);
  return {
    DOM: vdom$,
    events: events$,
  };
};

module.exports = Editor;

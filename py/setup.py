import setuptools

with open("../README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="soboro",
    version="0.0.1",
    author="Michael Jae-Yoon Chung",
    author_email="mjyc@cs.washington.edu",
    description="A SOcial roBOt behavioR authOring Script",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://github.com/mjyc/soboro",
    packages=["soboro"],
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires=">=3.6",
    install_requires=["Rx>=3.1.0"],
)
